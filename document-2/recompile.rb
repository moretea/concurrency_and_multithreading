#!/usr/bin/env ruby
# encoding: UTF-8

DOCUMENT = "assignment.tex"
INPUTS = Dir["sections/**/*.tex"] + [DOCUMENT]
FIGURES = Dir["figures/**/*.dia"]

require "rubygems"
require "bundler"
Bundler.setup

require "colorize"
require "rb-inotify"

class Recompiler
  def initialize document, inputs, figures
    @document = document
    @inputs   = inputs
    @figures  = figures
  end

  def run!
    initial_compile

    watch_loop
  end

  def watch_loop
    notifier = INotify::Notifier.new
    loop do
      @inputs.each do |input|
        notifier.watch(input, :modify) do
          compile_document
        end
      end

      @figures.each do |figure|
        notifier.watch(figure, :modify) do |event|
          compile_figure(event.absolute_name)
          compile_document
        end
      end

      notifier.process
    end
  end

  def initial_compile
    @figures.each do |figure|
      compile_figure figure
    end

    compile_document
  end

  def compile_figure path
    system("./dia2pdf #{path}")
  end

  def compile_document
    5.times do
      system("pdflatex -halt-on-error #{DOCUMENT}")
      if $? != 0
        puts "COMPILE ERROR".red
      else
        puts "SUCCESS".green
      end
    end
  end
end

Recompiler.new(DOCUMENT, INPUTS, FIGURES).run!
