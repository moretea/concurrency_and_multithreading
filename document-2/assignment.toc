\contentsline {section}{\numberline {1}Introduction}{3}
\contentsline {section}{\numberline {2}Implementation}{4}
\contentsline {subsection}{\numberline {2.1}Linked lists}{4}
\contentsline {subsection}{\numberline {2.2}Trees}{4}
\contentsline {section}{\numberline {3}Method}{5}
\contentsline {section}{\numberline {4}Results}{6}
\contentsline {subsection}{\numberline {4.1}Overview}{6}
\contentsline {subsection}{\numberline {4.2}Lists}{7}
\contentsline {subsection}{\numberline {4.3}Trees}{8}
\contentsline {subsubsection}{\numberline {4.3.1}Scaled to fit everyting}{8}
\contentsline {subsubsection}{\numberline {4.3.2}Coarse grained}{9}
\contentsline {section}{\numberline {5}Conclusions}{10}
\contentsline {section}{\numberline {6}Discussion}{11}
