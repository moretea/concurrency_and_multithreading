package data_structures;



import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.BrokenBarrierException;



public class WorkerThread extends Thread {


	private int id;
	private int nrIterations;
	private Sorted<Integer> sorted;
	private int[] itemsToAdd;
	private int[] itemsToRemove;
	private int workTime;
	private boolean doWork;
	private CyclicBarrier barrier;

  public static boolean DO_LOG = false;
	
	WorkerThread(int id, Sorted<Integer> list, int nrIterations, int[]
		itemsToAdd, int[] itemsToRemove, int workTime, CyclicBarrier barrier) {
		this.sorted = list;
		this.id = id;
		this.nrIterations = nrIterations;
		this.itemsToAdd = itemsToAdd;
		this.itemsToRemove = itemsToRemove;
		this.workTime = workTime;
		this.doWork = workTime > 0;
		this.barrier = barrier;
	}

	public void run() {
		int startIndex = nrIterations * id;
		add(sorted, startIndex, nrIterations, itemsToAdd);

		try {
		  if ((barrier.await() == 0) && DO_LOG) {
	        synchronized(System.out) {
	          System.out.println();
	          System.out.println("data structure before removal");
	          System.out.println(sorted);
	          System.out.println();
	          System.out.println();
	        }
		  }
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
		catch (BrokenBarrierException e) {
		    e.printStackTrace();
		}

		try {
			remove(sorted, startIndex, nrIterations, itemsToRemove);
		} catch (Throwable e) {
      System.out.println("Should be added");
      for (int i = 0; i < itemsToAdd.length; i++) {
        System.out.format("%d\n", itemsToAdd[i]);
      }

      System.out.println(sorted);
			e.printStackTrace();
			System.exit(-1);
		}
	}

	private void remove(Sorted<Integer> sorted, int startIndex, int nrIterations, int[] itemsToRemove) throws Throwable {
    long id = Thread.currentThread().getId();

		for (int i = startIndex; i < startIndex + nrIterations; i++) {
			doWork();

	      if (DO_LOG)
	      synchronized(System.out) {
	        System.out.format("[%d] Remove %d\n", id, itemsToRemove[i]);
	      }
	
	      //try {
	    	  sorted.remove(itemsToRemove[i]);
	      /*} catch (Exception e) {
	        synchronized(System.out) {
	          System.out.println("Exception during removal:");
	          e.printStackTrace();
	        }
	      }
	*/
	      if (DO_LOG)
	      synchronized(System.out) {
	        System.out.format("[%d] Removed %d\n", id, itemsToRemove[i]);
	        System.out.println(sorted.toString());
	        System.out.println();
	      }
		}
	}

	private void add(Sorted<Integer> sorted, int startIndex, int nrIterations, int[] itemsToAdd) {
    long id = Thread.currentThread().getId();

		for (int i = startIndex; i < startIndex + nrIterations; i++) {
			doWork();

      if (DO_LOG)
      synchronized(System.out) {
        System.out.format("[%d] Adding %d\n", id, itemsToAdd[i]);
      }

			sorted.add(itemsToAdd[i]);

      if (DO_LOG)
      synchronized(System.out) {
        System.out.format("[%d] Added %d\n", id, itemsToAdd[i]);
      }
      //System.out.println(sorted.toString());
      //System.out.println();
		}
	}

	private void doWork() {
		if (doWork) {
			long end = System.nanoTime() + workTime * 1000;
			while (System.nanoTime() < end); // busy wait
		}
	}
}
