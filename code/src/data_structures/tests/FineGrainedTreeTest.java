package data_structures.tests;
import data_structures.implementation.*;

import org.junit.* ;
import static org.junit.Assert.* ;
import data_structures.tests.CoarseGrainedTreeTest;

public class FineGrainedTreeTest extends CoarseGrainedTreeTest {

  public void treeMustBeUnlocked(FineGrainedTree<Integer> tree) {
    assertTrue(tree.rootLock.isLocked() == false);
    if (tree.root != null)
      treeMustBeUnlocked(tree.root);
  }

  public void treeMustBeUnlocked(FineGrainedTree<Integer>.Node<Integer> node) {
    assertTrue(node.nodeLock.isLocked() == false);

    if (node.left != null)
      treeMustBeUnlocked(node.left);
    
    if (node.right!= null)
      treeMustBeUnlocked(node.right);
  }

  //// UNLOCK TESTS
  @Test
  public void ensure_all_locks_are_free_empty() {
    FineGrainedTree<Integer> tree = new FineGrainedTree<Integer>();

    treeMustBeUnlocked(tree);
  }

  @Test
  public void ensure_after_add_unlocked_1() {
    FineGrainedTree<Integer> tree = new FineGrainedTree<Integer>();

    tree.add(1);
    treeMustBeUnlocked(tree);
  }

  @Test
  public void ensure_after_add_unlocked_2() {
    FineGrainedTree<Integer> tree = new FineGrainedTree<Integer>();

    tree.add(1);
    tree.add(2);
    treeMustBeUnlocked(tree);
  }

  @Test
  public void ensure_after_add_unlocked_3() {
    FineGrainedTree<Integer> tree = new FineGrainedTree<Integer>();

    tree.add(3);
    tree.add(1);
    tree.add(2);
    treeMustBeUnlocked(tree);
  }


  //REMOVE

  @Test
  public void ensure_after_remove_unlocked() throws Throwable {
    FineGrainedTree<Integer> tree = new FineGrainedTree<Integer>();
    tree.add(3);
    tree.add(1);
    tree.add(2);

    tree.remove(3);
    treeMustBeUnlocked(tree);
  }

  @Test
  public void remove_root_correctly_please() throws Throwable {
    FineGrainedTree<Integer> tree = new FineGrainedTree<Integer>();
    tree.add(1);
    tree.add(6);
    tree.add(9);

    tree.remove(1);
    treeMustBeUnlocked(tree);

    assertTrue(tree.root.value == 6);
    assertTrue(tree.root.left == null);

    assertTrue(tree.root.right.value == 9);
    assertTrue(tree.root.right.left == null);
    assertTrue(tree.root.right.right == null);
  }

  @Test
  @Ignore
  public void remove_all_values_from_the_tree_reverse_order() throws Throwable {
    FineGrainedTree<Integer> tree = new FineGrainedTree<Integer>();
    int n = 300;
    for(int i =1; i <= n; i++) {
      tree.add(i);
    }

    for(int i = n; i > 0; i--) {
      tree.remove(i);
    }
  }

  @Test
  @Ignore
  public void remove_all_values_from_the_tree_same_order() throws Throwable {
    FineGrainedTree<Integer> tree = new FineGrainedTree<Integer>();
    int n = 300;
    for(int i =1; i <= n; i++) {
      tree.add(i);
    }

    for(int i =1; i <= n; i++) {
      if (i == 128) {
        System.out.println(tree.toString(10));
      }
      tree.remove(i);
    }
  }

  @Test
  public void another_failing_example() throws Throwable {
    FineGrainedTree<Integer> tree = new FineGrainedTree<Integer>();
    tree.add(128);
    tree.add(129);
    tree.add(130);
    tree.add(131);

    tree.remove(128);
  }

  @Test
  public void another_failing_example_b() throws Throwable {
    FineGrainedTree<Integer> tree = new FineGrainedTree<Integer>();
    tree.add(4);
    tree.add(1);

    tree.remove(4);

    assertTrue(tree.root.value == 1);
    should_be_null(tree.root.left);
    should_be_null(tree.root.right);
  }

  @Test
  public void another_failing_example_c() throws Throwable {
    FineGrainedTree<Integer> tree = new FineGrainedTree<Integer>();

    tree.add(7);
    tree.add(1);
    tree.add(1);
    tree.add(5);
    tree.add(4);

    tree.remove(1);

    se(7, tree.root.value);
    se(4, tree.root.left.value);
    se(1, tree.root.left.left.value);
    se(5, tree.root.left.right.value);

    should_be_null(tree.root.left.right.left);
    should_be_null(tree.root.left.right.right);
  }

  public void se(int x, int y) {
    if (x != y) {
      System.out.format("%d IS NOT %d\n", x, y);
      assertTrue(x == y);
    }
  }

  public void should_be_null(FineGrainedTree<Integer>.Node<Integer> node) {
    assertTrue(node == null);
  }
}
