package data_structures.tests;
import data_structures.implementation.*;

import org.junit.* ;
import static org.junit.Assert.* ;

public class CoarseGrainedTreeTest {

  ////////////////////// ADD TESTS
  @Test
  public void test_add_root()  throws Throwable {
    CoarseGrainedTree<Integer> tree = new CoarseGrainedTree<Integer>();
    assertTrue(tree.root == null);

    tree.add(1);
    assertTrue(tree.root != null);
    assertTrue(tree.root.value == 1);
  }

  @Test
  public void test_add_two()  throws Throwable {
    CoarseGrainedTree<Integer> tree = new CoarseGrainedTree<Integer>();
    assertTrue(tree.root == null);

    tree.add(2);
    tree.add(1);

    assertTrue(tree.root != null);
    assertTrue(tree.root.value == 2);

    assertTrue(tree.root.left.value == 1);
  }

  ////////////////////// REMOVE TESTS

  /**** ROOOT ****/
  @Test
  public void test_RemoveRoot()  throws Throwable {
    CoarseGrainedTree<Integer> tree = new CoarseGrainedTree<Integer>();

    tree.add(1);
    assertTrue(tree.root.value == 1);

    tree.remove(1);
    assertTrue(tree.root == null);
  }

  @Test
  public void test_removeRoot_withRightChild()  throws Throwable {
    CoarseGrainedTree<Integer> tree = new CoarseGrainedTree<Integer>();

    /*
     *      1        <=== remove 1
     *            2
     *        null null
     *
     */

    tree.add(1);
    tree.add(2);

    assertTrue(tree.root.value == 1);
    assertTrue(tree.root.right.value == 2);

    tree.remove(1);
    assertTrue(tree.root.value == 2);
  }

  @Test
  public void test_removeRoot_withRightSubtree()  throws Throwable {
    CoarseGrainedTree<Integer> tree = new CoarseGrainedTree<Integer>();
    
    /*
     *      59
     *        \
     *        295
     *      /   \
     *    155  545
     *     \
     *      220
     *
     */


    tree.add(59);
    tree.add(295);
    tree.add(545);
    tree.add(155);
    tree.add(220);

    /* Make sure that the insert is OK */
    assertTrue(tree.root.value == 59);
    assertTrue(tree.root.right.value == 295);
    assertTrue(tree.root.right.left.value == 155);
    assertTrue(tree.root.right.left.right.value == 220);
    assertTrue(tree.root.right.right.value == 545);

    tree.remove(59);

    assertTrue(tree.root.value == 155);
    assertTrue(tree.root.right.value == 295);
    assertTrue(tree.root.right.left.value == 220);
    assertTrue(tree.root.right.right.value == 545);
  }


  @Test
  public void test_removeRoot_withLeftChild()  throws Throwable {
    CoarseGrainedTree<Integer> tree = new CoarseGrainedTree<Integer>();

    tree.add(2);
    tree.add(1);

    assertTrue(tree.root.value == 2);
    tree.remove(2);

    assertTrue(tree.root.value == 1);
  }

  @Test
  public void test_remove_root_from_semi_linked_list()  throws Throwable {
    CoarseGrainedTree<Integer> tree = new CoarseGrainedTree<Integer>();

    /*
     * 1
     *  \
     *   2
     *    \
     *     3
     *      \
     *       4
     */

    tree.add(1);
    tree.add(2);
    tree.add(3);
    tree.add(4);

    assertTrue(tree.root.value == 1);
    assertTrue(tree.root.right.value == 2);
    assertTrue(tree.root.right.right.value == 3);
    assertTrue(tree.root.right.right.right.value == 4);

    tree.remove(1);

    assertTrue(tree.root.value == 2);
    assertTrue(tree.root.right.value == 3);
    assertTrue(tree.root.right.right.value == 4);

  }
  
  /*** LEAF NODES ****/
  public void test_remove_left_node_no_right_node()  throws Throwable {
    CoarseGrainedTree<Integer> tree = new CoarseGrainedTree<Integer>();

    /*
     *       A
     *     B   C
     *   D
     * 
     *  Remove D
     */
    tree.add(10); // A
    tree.add(5); // B
    tree.add(15); // C
    tree.add(3); // D

    assertTrue(tree.root.left.left != null);
    tree.remove(3);
    assertTrue(tree.root.left.left == null);
  }

  public void test_remove_left_node_no_left_node()  throws Throwable {
    CoarseGrainedTree<Integer> tree = new CoarseGrainedTree<Integer>();

    /*
     *       A
     *     B   C
     *       D
     * 
     *  Remove D
     */
    tree.add(10); // A
    tree.add(5); // B
    tree.add(15); // C
    tree.add(7); // D

    assertTrue(tree.root.left.left != null);
    tree.remove(7);
    assertTrue(tree.root.left.left == null);
  }

  /** INNER NODES ***/
  public void test_remove_inner_node_no_right_node()  throws Throwable {
    CoarseGrainedTree<Integer> tree = new CoarseGrainedTree<Integer>();

    /*
     *       A
     *     B   C
     *   D
     * 
     *  Remove B
     */
    tree.add(10); // A
    tree.add(5); // B
    tree.add(15); // C
    tree.add(3); // D

    assertTrue(tree.root.left.value == 5);
    assertTrue(tree.root.left.left != null);
    tree.remove(5);
    assertTrue(tree.root.left.value == 3);
    assertTrue(tree.root.left.left == null);
  }

  public void test_remove_inner_node_no_left_node()  throws Throwable {
    CoarseGrainedTree<Integer> tree = new CoarseGrainedTree<Integer>();

    /*
     *       A
     *     B   C
     *       D
     * 
     *  Remove B
     */
    tree.add(10); // A
    tree.add(5); // B
    tree.add(15); // C
    tree.add(7); // D

    assertTrue(tree.root.left.value == 5);
    assertTrue(tree.root.left.right != null);
    tree.remove(5);
    assertTrue(tree.root.left.value == 7);
    assertTrue(tree.root.left.right == null);
  }

  // INTERNAL FUNCTIONS
  @Test
  public void test_removeSmallestFromRightSubTreeAndReturn_Flat()  throws Throwable {
    CoarseGrainedTree<Integer> tree = new CoarseGrainedTree<Integer>();
    /*
     * 1
     *  \
     *   2
     */

    tree.add(1);
    tree.add(2);

    Integer i = tree.removeSmallestFromRightSubTreeAndReturn(tree.root);
    assertTrue(i == 2);
    assertTrue(tree.root.right == null);
  }

  @Test
  public void test_removeSmallestFromRightSubTreeAndReturn_Deep()  throws Throwable {
    CoarseGrainedTree<Integer> tree = new CoarseGrainedTree<Integer>();

    /*
     *    59
     *   /  \
     *  ~    295 
     *      /   \
     *    155   545
     *   /   \
     *  ~    220
     */

    tree.add(59);
    tree.add(295);
    tree.add(155);
    tree.add(220);
    tree.add(545);

    Integer i = tree.removeSmallestFromRightSubTreeAndReturn(tree.root);
    assertTrue(i == 155);

    assertTrue(tree.root.right.left.left  == null);
    assertTrue(tree.root.right.left.right == null);
    
    assertTrue(tree.root.right.left.value == 220);
  }
}
