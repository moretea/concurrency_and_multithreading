package data_structures.implementation;

import data_structures.Sorted;
import data_structures.WorkerThread;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.atomic.AtomicInteger;

import java.lang.Thread;

public class FineGrainedTree<T extends Comparable<T>> implements Sorted<T> {

  private static int getLineNumber(int idx) {
    StackTraceElement[] stackTraces = Thread.currentThread().getStackTrace();
    StackTraceElement stackTraceElement = stackTraces[idx];
    return stackTraceElement.getLineNumber();
  }

  public static enum NodeType {
    Leaf,
    OnlyLeft,
    OnlyRight,
    FullInner
  }

  public class Node<T> {
    public volatile int id;

    public volatile T value;
    public volatile Node<T> left;
    public volatile Node<T> right;
    public volatile ReentrantLock nodeLock;


    public Node(T value) {
      this.id =  (Integer) value + 200;
      this.left  = null;
      this.right = null;
      this.value = value;
      this.nodeLock = new ReentrantLock();
    }

    public NodeType getNodeType() {
      if (this.right != null && this.right != null)
        return NodeType.FullInner;
      else if (this.right == null && this.left != null)
        return NodeType.OnlyLeft;
      else if (this.right != null && this.left == null)
        return NodeType.OnlyRight;
      else /*if (this.left == null && this.right == null) */
        return NodeType.Leaf;
    }

    public void lock(String msg) {
      long id = Thread.currentThread().getId();
      int nr  = getLineNumber(4);

      if (WorkerThread.DO_LOG)
      synchronized(System.out) {
        System.out.printf("[%d]:%d Lock %d %s %s\n", id, nr, this.id, this.value.toString(), msg);
      }

      this.nodeLock.lock();

      if (WorkerThread.DO_LOG)
      synchronized(System.out) {
        System.out.printf("[%d]:%d Lock %d acquired %s %s\n", id, nr, this.id, this.value.toString(), msg);
      }
    }

    public void unlock(String msg) {
      long id = Thread.currentThread().getId();
      int nr  = getLineNumber(4);

      if (WorkerThread.DO_LOG)
      synchronized(System.out) {
        System.out.printf("[%d]:%d Unlock %d %s %s\n", id, nr, this.id, this.value.toString(), msg);
      }
      this.nodeLock.unlock();
    }

    /* Inorder tree traversal */
    public String toString() {
      return toString("", true, null, -1);
    }

    public String toString(int max_depth) {
      return toString("", true, null, max_depth);
    }

    public String toString(String ident, Boolean last, Boolean direction, int depth_remaining) {
      String result = "";

      if (depth_remaining != -1) {

        if (depth_remaining == 0) {
          return "";
        } else {
          depth_remaining -= 1;
        }
      }

      if (last) {
        result += ident + " └── ";
        ident += "    ";
      } else {
        result += ident + " ├── ";
        if (direction == null) {
          ident += "    ";
        } else {
          ident += " │  ";
        }
      }

      if (direction != null) {
        if (direction == true) { // left
          result += "L ";
        } else if (direction == false) { //right
          result += "R ";
        }
      }

      result += String.format("(%d)", this.id) + this.value.toString() + "\n";

      if (this.left != null) {
        result += this.left.toString(ident, this.right == null, true, depth_remaining);
      }

      if (this.right != null) {
        result += this.right.toString(ident, true, false, depth_remaining);
      }

      return result;
    }
  }

  public volatile Node<T> root;
  public volatile ReentrantLock rootLock;

  public FineGrainedTree() {
    this.rootLock = new ReentrantLock();
  }

  private void lockRoot() {
    long id = Thread.currentThread().getId();
    int nr  = getLineNumber(3);

    if (WorkerThread.DO_LOG)
    synchronized(System.out) {
      System.out.format("[%d]:%d Lock ROOT\n", id, nr);
    }

    this.rootLock.lock();

    if (WorkerThread.DO_LOG)
    synchronized(System.out) {
      System.out.format("[%d]:%d Lock ROOT acquired\n", id, nr);
    }
  }

  private void unlockRoot() {
    long id = Thread.currentThread().getId();
    int nr  = getLineNumber(3);

    if (WorkerThread.DO_LOG)
    synchronized(System.out) {
      System.out.format("[%d]:%d Unlock ROOT\n", id, nr);
    }

    this.rootLock.unlock();
  }

  private void log(String msg) {
    long id = Thread.currentThread().getId();
    int nr  = getLineNumber(3);

    if (WorkerThread.DO_LOG)
    synchronized(System.out) {
      System.out.printf("[%d]:%d LOG %s\n", id, nr, msg);
    }
  }

	public void add(T t) {
    this.lockRoot();
    Node<T> newNode = new Node<T>(t);

    if (this.root == null) {
      this.root = newNode;
      this.unlockRoot();
    } else {
      Node<T> previous = null;
      Node<T> current  = this.root;
      current.lock("current");
      this.unlockRoot();

      while(true) {
        if (newNode.value.compareTo(current.value) <= 0) { // Left child
          if(current.left == null) {
            current.left = newNode;
            break;
          } else {
            previous = current;
            current  = current.left;
            current.lock("current");
            previous.unlock("previous");
            continue;
          }
        } else { // Right child
          if(current.right == null) {
            current.right = newNode;
            break;
          } else {
            previous = current;
            current  = current.right;
            current.lock("current");
            previous.unlock("previous");
            continue;
          }
        }
      }

      current.unlock("current");
    }
	}

 	public void remove(T value) throws Throwable {
    this.lockRoot();
    this.root.lock("root node");

    if (this.root.value.compareTo(value) == 0) { // Remove the root
      removeRoot();
    } else {
      this.unlockRoot();
      removeNonRoot(value);
    }
	}

 private void removeRoot() {
    Node<T> oldRoot = this.root;

    if (this.root.right == null) {
      this.root = this.root.left;
    } else {
      this.root.value = removeSmallestFromRightSubTreeAndReturn(this.root);
    }

    this.unlockRoot();
    oldRoot.unlock("root");
  }

  enum RemoveDirection {
    Found, Left, Right
  }

  public RemoveDirection removeCompare(Node<T> parent, T value) throws Throwable {
    switch(value.compareTo(parent.value)) {
      case  0: return RemoveDirection.Found;
      case -1: return RemoveDirection.Left;
      case  1: return RemoveDirection.Right;
    }

    throw new Exception("Unexpected comparision value!");
  }

  public void removeNonRoot(T value) throws Throwable {
    boolean rootLocked = true;

    Node<T> parent  = this.root;
    Node<T> current = null;

    switch(removeCompare(parent, value)) {
      case Found: throw new Exception("Erm, this should have been handled by removeRoot()");
      case Left:  current = parent.left; break;
      case Right: current = parent.right; break;
    }

    current.lock("");

    while(true) {
      RemoveDirection direction = removeCompare(current, value);

      if (direction == RemoveDirection.Found) {
        doRemoveNonRoot(parent, current);
        return;
      } else {
        Node<T> oldParent = parent;
        parent            = current;

        if (direction == RemoveDirection.Left) {
          current = parent.left;
        } else { /* direction == RemoveDirection.right */
          current = parent.right;
        }

        if (current == null) {
          throw new Exception(String.format("The value %s was not found!", value.toString()));
        }

        current.lock("");
        oldParent.unlock("");
      }
    }
  }

  public void doRemoveNonRoot(Node<T> parent, Node<T> toRemove) {
    switch(toRemove.getNodeType()) {
      case Leaf:
       replaceChild(parent, toRemove, null);
       break;
      case OnlyLeft:
        replaceChild(parent, toRemove, toRemove.left);
        break;
      case OnlyRight:
        replaceChild(parent, toRemove, toRemove.right);
        break;
      case FullInner:
        toRemove.value = removeSmallestFromRightSubTreeAndReturn(toRemove);
        break;
    }
    parent.unlock("paRENT");
    toRemove.unlock("current");
  }

	public String toString() {
    return toString(-1);
	}

	public String toString(int maxDepth) {
    if (root == null) {
      return "empty";
    } else {
      return root.toString(maxDepth);
    }
	}

  public T removeSmallestFromRightSubTreeAndReturn(Node<T> originalNode) {
    Node<T> parent  = originalNode;
    Node<T> current = parent.right;
    current.lock("");

    Node<T> next = current.left;

    while (next != null) {
      if (parent != originalNode) {
        parent.unlock("");
      }

      parent = current;
      current = next;
      current.lock("");

      next = current.left;
    }

    replaceChild(parent, current, current.right);
    T value = current.value;

    current.unlock("");

    if (parent != originalNode) {
      parent.unlock("");
    }

    return value;
  }

  private void replaceChild(Node<T> parent, Node<T> child, Node<T> with) {
    if(parent.left == child) {
      parent.left = with;
    } else if (parent.right == child) {
      parent.right = with;
    } else {
      throw new UnsupportedOperationException();
    }
  }
}
