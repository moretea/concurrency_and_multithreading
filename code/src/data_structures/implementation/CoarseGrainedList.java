package data_structures.implementation;

import data_structures.Sorted;
import java.util.concurrent.locks.*;

public class CoarseGrainedList<T extends Comparable<T>> implements Sorted<T> {
	private Lock globalLock;
	private ListLink<T> head;

	public CoarseGrainedList() {
		globalLock = new ReentrantLock();
	}

	public void add(T t) {
		ListLink<T> newLink = new ListLink<T>(t);
		globalLock.lock();
		
		try {
			if (head == null) {								// Case 1: The empty list
				head = newLink;
			} else {
				if (this.head.value.compareTo(t) >= 0) {	// Case 2: the new value before the first link
					newLink.next = head;
					head = newLink;
				} else {
					ListLink<T> current = head;
					ListLink<T> next = head.next;
					
					while (next != null) {
						if (next.value.compareTo(t) >= 0) {	//Case 3:the new value should be added somewhere in the middle of the list
							newLink.next = next;
							current.next = newLink;
							return;
						}
						
						current = next;
						next = current.next;
					}
					
					current.next = newLink;					//Case 4: end of the list
				}
			}
		} finally {
			globalLock.unlock();
		}
	}
	
	public void remove(T t){
		globalLock.lock();
		
		try {
			if (head.value.compareTo(t) == 0) {				//Case 1: the remove value is at the head of the list
				head = head.next;
				return;
			} else {
				ListLink<T> current = head;
				ListLink<T> next = head.next;
				
				while (next != null) {
					if (next.value.compareTo(t) >= 0) {		//Case 2: remove value is in the middle or at end of the list	
						current.next = next.next;
						return;
					}
					
					current = next;
					next = current.next;
				}			
			}
		} finally {			
			globalLock.unlock();
		}
	}
	
	public String toString(){
		globalLock.lock();		
		String result = "";

		if (head == null){
			return "empty";
		}
		
		try{			
			ListLink<T> current = head;			
			
			while (current != null) {
				result += current.value + " ";
				current = current.next;
			}
		} finally {
			globalLock.unlock();
		}
		
		return result;
	}
	
	@SuppressWarnings("hiding")
	class ListLink<T> {
		public T value;
		public ListLink<T> next;

		public ListLink(T t) {
			this.value = t;
		}
	}
}
