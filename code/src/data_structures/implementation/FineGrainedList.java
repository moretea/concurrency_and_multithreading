package data_structures.implementation;

import java.util.concurrent.locks.ReentrantLock;

import data_structures.Sorted;

public class FineGrainedList<T extends Comparable<T>> implements Sorted<T> {
	private ListLink<T> head;

	public FineGrainedList() {
		head = new ListLink<T>(null);
	}

	public void add(T t) {
		ListLink<T> pred = head;
		ListLink<T> newLink = new ListLink<T>(t);
		head.lock();
		
		try {		
			if (head.next == null) {		// Case 1: The empty list
				head.next = newLink;
				return;
			}

			ListLink<T> curr = pred.next;
			curr.lock();
			try {
				while (curr != null && curr.value.compareTo(t) < 0) { //Search in case of case 3 (the new value should be added somewhere in the middle of the list). Is skipped in case 2: the new value before the first link
					pred.linkLock.unlock();
					pred = curr;
					curr = curr.next;
					
					if (curr != null) {
						curr.lock();
					}
				}
				
				newLink.next = curr;				//Goes well in case 2, 3 and 4 (end of the list)
				pred.next = newLink;
			} finally {
				if (curr != null) {
					curr.unlock();
				}
			}
		} finally {
			pred.unlock();
		}
	}

	public void remove(T t) {
		ListLink<T> pred, curr;
		pred = head;	//cannot be removed or altered; no lock needed
		pred.lock();
		
		try {
			curr = pred.next;
			curr.lock();
			try {
				while (curr != null && curr.value.compareTo(t) < 0) {
					pred.unlock();
					pred = curr;
					curr = curr.next;
					
					if(curr != null){
						curr.lock();
					}
				}
				
				if (curr.value.compareTo(t) == 0) {
					pred.next = curr.next;
				}
			} finally {
				if(curr != null){
					curr.unlock();
				}
			}
		} finally {
			pred.unlock();
		}
	}

	public String toString(){    //TODO: still coarse grained
		ListLink<T> pred = head;
		pred.lock();
		
		String result = "";

		try {
			if (head.next == null) {
				return result + "empty";
			}
			
			ListLink<T> current = head.next;
			current.lock();
			  
			try {
				while (current != null) {
					pred.unlock();
					result += current.toString() + " ";
					pred = current;
					current = current.next;
					
					if (current != null) {
						current.lock();
					}
				}
			} finally{
				if (current != null) {
					current.unlock();
				}
			}
		} finally {
			pred.unlock();
		}

		return result;
	}

	@SuppressWarnings("hiding")
	class ListLink<T> {
		public T value;
		public ListLink<T> next;
		private ReentrantLock linkLock;
		
		public ListLink(T t) {
			this.value = t;
			linkLock = new ReentrantLock();
		}
		
		public void lock(){
			linkLock.lock();
		}
		
		public void unlock(){
			linkLock.unlock();
		}
		
		public String toString(){
			return value.toString();
		}
	}
}
