package data_structures.implementation;

import data_structures.Sorted;
import java.util.concurrent.locks.*;

public class CoarseGrainedTree<T extends Comparable<T>> implements Sorted<T> {
  private static int getLineNumber(int idx) {
    StackTraceElement[] stackTraces = Thread.currentThread().getStackTrace();
    StackTraceElement stackTraceElement = stackTraces[idx];
    return stackTraceElement.getLineNumber();
  }

  public static enum NodeType {
    Leaf,
    OnlyLeft,
    OnlyRight,
    FullInner
  }

  public class Node<T> {
    public volatile int id;

    public volatile T value;
    public volatile Node<T> left;
    public volatile Node<T> right;

    public Node(T value) {
      this.id =  (Integer) value + 200;
      this.left  = null;
      this.right = null;
      this.value = value;
    }

    public NodeType getNodeType() {
      if (this.right != null && this.right != null)
        return NodeType.FullInner;
      else if (this.right == null && this.left != null)
        return NodeType.OnlyLeft;
      else if (this.right != null && this.left == null)
        return NodeType.OnlyRight;
      else /*if (this.left == null && this.right == null) */
        return NodeType.Leaf;
    }

    /* Inorder tree traversal */
    public String toString() {
      return toString("", true, null, -1);
    }

    public String toString(int max_depth) {
      return toString("", true, null, max_depth);
    }

    public String toString(String ident, Boolean last, Boolean direction, int depth_remaining) {
      String result = "";

      if (depth_remaining != -1) {

        if (depth_remaining == 0) {
          return "";
        } else {
          depth_remaining -= 1;
        }
      }

      if (last) {
        result += ident + " └── ";
        ident += "    ";
      } else {
        result += ident + " ├── ";
        if (direction == null) {
          ident += "    ";
        } else {
          ident += " │  ";
        }
      }

      if (direction != null) {
        if (direction == true) { // left
          result += "L ";
        } else if (direction == false) { //right
          result += "R ";
        }
      }

      result += String.format("(%d)", this.id) + this.value.toString() + "\n";

      if (this.left != null) {
        result += this.left.toString(ident, this.right == null, true, depth_remaining);
      }

      if (this.right != null) {
        result += this.right.toString(ident, true, false, depth_remaining);
      }

      return result;
    }
  }

  public volatile Node<T> root;
  public volatile ReentrantLock treeLock;

  public CoarseGrainedTree() {
    this.treeLock = new ReentrantLock();
  }

	public void add(T t) {
    Node<T> newNode = new Node<T>(t);

    this.treeLock.lock();
    try {
      if (this.root == null) {
        this.root = newNode;
      } else {
        Node<T> previous = null;
        Node<T> current  = this.root;

        while(true) {
          if (newNode.value.compareTo(current.value) <= 0) { // Left child
            if(current.left == null) {
              current.left = newNode;
              break;
            } else {
              previous = current;
              current  = current.left;
              continue;
            }
          } else { // Right child
            if(current.right == null) {
              current.right = newNode;
              break;
            } else {
              previous = current;
              current  = current.right;
              continue;
            }
          }
        }
      }
    } finally {
      this.treeLock.unlock();
    }
	}

 	public void remove(T value) throws Throwable {
    this.treeLock.lock();

    try {
      if (this.root.value.compareTo(value) == 0) { // Remove the root
        removeRoot();
      } else {
        removeNonRoot(value);
      }
    } finally {
      this.treeLock.unlock();
    }
	}

 private void removeRoot() {
    Node<T> oldRoot = this.root;

    if (this.root.right == null) {
      this.root = this.root.left;
    } else {
      this.root.value = removeSmallestFromRightSubTreeAndReturn(this.root);
    }

  }

  enum RemoveDirection {
    Found, Left, Right
  }

  public RemoveDirection removeCompare(Node<T> parent, T value) throws Throwable {
    switch(value.compareTo(parent.value)) {
      case  0: return RemoveDirection.Found;
      case -1: return RemoveDirection.Left;
      case  1: return RemoveDirection.Right;
    }

    throw new Exception("Unexpected comparision value!");
  }

  public void removeNonRoot(T value) throws Throwable {

    Node<T> parent  = this.root;
    Node<T> current = null;

    switch(removeCompare(parent, value)) {
      case Found: throw new Exception("Erm, this should have been handled by removeRoot()");
      case Left:  current = parent.left; break;
      case Right: current = parent.right; break;
    }

    while(true) {
      RemoveDirection direction = removeCompare(current, value);

      if (direction == RemoveDirection.Found) {
        doRemoveNonRoot(parent, current);
        return;
      } else {
        Node<T> oldParent = parent;
        parent            = current;

        if (direction == RemoveDirection.Left) {
          current = parent.left;
        } else { /* direction == RemoveDirection.right */
          current = parent.right;
        }

        if (current == null) {
          throw new Exception(String.format("The value %s was not found!", value.toString()));
        }
      }
    }
  }

  public void doRemoveNonRoot(Node<T> parent, Node<T> toRemove) {
    switch(toRemove.getNodeType()) {
      case Leaf:
       replaceChild(parent, toRemove, null);
       break;
      case OnlyLeft:
        replaceChild(parent, toRemove, toRemove.left);
        break;
      case OnlyRight:
        replaceChild(parent, toRemove, toRemove.right);
        break;
      case FullInner:
        toRemove.value = removeSmallestFromRightSubTreeAndReturn(toRemove);
        break;
    }
  }

	public String toString() {
    return toString(-1);
	}

	public String toString(int maxDepth) {
    if (root == null) {
      return "empty";
    } else {
      return root.toString(maxDepth);
    }
	}

  public T removeSmallestFromRightSubTreeAndReturn(Node<T> originalNode) {
    Node<T> parent  = originalNode;
    Node<T> current = parent.right;

    Node<T> next = current.left;

    while (next != null) {
      parent = current;
      current = next;

      next = current.left;
    }

    replaceChild(parent, current, current.right);
    T value = current.value;

    return value;
  }

  private void replaceChild(Node<T> parent, Node<T> child, Node<T> with) {
    if(parent.left == child) {
      parent.left = with;
    } else if (parent.right == child) {
      parent.right = with;
    } else {
      throw new UnsupportedOperationException();
    }
  }
}
