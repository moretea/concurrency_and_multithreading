$ids = {}

IDS=%W{A B C D E F G}

def ident match
  if not $ids.has_key? match
    $ids[match] = IDS.shift
  end

  "#{$ids[match]} #{match}"
end

class State
  class Draadje
    attr_reader :n
    attr_accessor :action
    attr_accessor :locks_waiting
    attr_accessor :locks

    def initialize(n)
     @n = n
     @action = nil

     @locks_waiting = []
     @locks = []
    end
  end

  def initialize
    @t = {}
  end

  def add(n, nr)
    @t[n] ||= Draadje.new(n)

    @t[n].action = [:add, nr]
  end

  def added(n)
    @t[n].action = nil
  end

  def try_lock(n, what)
    @t[n].locks_waiting << what
  end

  def acquire_lock(n, what)
    @t[n].locks_waiting.delete what
    @t[n].locks << what
  end

  def unlock(n, what)
    @t[n].locks.delete(what)
  end

  def print_state
    @t.values.each do |thread|
      puts thread.n
      puts "Waiting for: #{thread.locks_waiting}"
      puts "Holding:     #{thread.locks}"
      puts
    end
  end
end

state = State.new
File.readlines("trace.txt").each do |line|
  case line
  when /(\d) Add (.*)/      
    state.add($1, $2)
    puts("#{$1} Add " + ident($2))
  when /(\d)   Lock head/   
    state.try_lock $1, :head
    puts(line)
  when /(\d)   Lock head acquired/
    state.acquire_lock $1, :head
    puts(line)
  when /(\d)   Unlock head/ 
    state.unlock $1, :head
    puts(line)
  when /(\d)   Added (.*)/  
    state.added $1
    puts("#{$1}  Addedd " + ident($2))
  when /(\d)   Lock acquired (.*)/   
    state.acquire_lock $1, $2
    puts("#{$1}  Lock acquire "   + ident($2))
  when /(\d)   Lock (.*)/   
    state.try_lock $1, $2
    puts("#{$1}  Lock "   + ident($2))
  when /(\d)   Unlock (.*)/ 
    puts("#{$1}  Unlock " + ident($2))
    state.unlock $1, $2
  else 
    puts "UNKOWN" + line.inspect
  end
end

state.print_state
