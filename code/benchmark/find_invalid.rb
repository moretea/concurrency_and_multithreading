# Find invalid entries.

Dir["benchmark/data/*"].each do |data_structure|
  data_structure_name = data_structure.split("/").last

  Dir[data_structure + "/*"].each do |file|
    invalid = false
    Marshal.load(File.read(file)).each do |key, value|
      ds, nr_threads, nr_items = key
      avg = ((value.inject(:+).to_f / value.length) * 100).to_i.to_f / 100
      
      invalid = avg == 0

#      p [ds, nr_threads, nr_items, avg]
    end

    puts file if invalid
  end
end
