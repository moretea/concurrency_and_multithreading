DATA_STRUCTURES = ["cgl", "fgl", "cgt", "fgt"]
THREADS         = [1, 2, 4, 8]
ITEMS           = [8] + (128..32768).step(512).to_a 
DATA_STRUCTURE_NAMES = {"cgl" => "Coarse Grained List",
                        "fgl" => "Fine Grained List",
                        "cgt" => "Coarse Grained Tree",
                        "fgt" => "Fine Grained Tree" }

REPEAT_SAME_TEST = 5
