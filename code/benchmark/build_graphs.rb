$LOAD_PATH << Dir.pwd
require "rubygems"
require "benchmark/config.rb"
require "gnuplot"

class Graph
  def initialize(length, ds)
    @length = length
    @ds = ds 
    @results = {}
  end

  def add(threads, n, times)
    raise ArgumentError.new if times.length != @length
    @results[threads] ||= {}
    @results[threads][n] = times
  end

  def file_name
    File.expand_path("../report/figures/#{@ds}-#{@nr_threads}.pdf", __FILE__)
  end

  def plot options
    Gnuplot.open do |gp|
      Gnuplot::Plot.new( gp ) do |plot|
      
        # The following lines allow outputting the graph to an image file. 
        # The first set the kind of image that you want, while the second
        # redirects the output to a given file. 
        #
        # Typical terminals: gif, png, postscript, latex, texdraw
        #
        # See http://mibai.tec.u-ryukyu.ac.jp/~oshiro/Doc/gnuplot_primer/gptermcmp.html
        # for a list of recognized terminals.
        #
        plot.terminal "pdf color"
        plot.output options[:file]
      
        # see sin_wave.rb
        #plot.xrange "[-10:10]"
        plot.title  options[:title] || DATA_STRUCTURE_NAMES[@ds]
        plot.ylabel "T(ms)"
        plot.xlabel "N"
        plot.key "left top"

        plot.yrange "[0:#{options[:max_y]}]"

        @results.keys.sort.each do |n_threads|
          dataset_results = @results[n_threads]
          next if n_threads == 6

          items   = []
          results = []

          dataset_results.keys.sort.each do |n_items|
            items << n_items
            result = dataset_results[n_items].inject(:+).to_f / dataset_results[n_items].length
            results << result
          end

          plot.data << Gnuplot::DataSet.new([items, results]) do |ds|
            ds.with = "lines"
            ds.linewidth = 4
            ds.title = "#{n_threads} threads"
          end
        end
      end
    end
  end
end

# Build the graphs
graphs = {}
Dir["benchmark/data/*"].each do |data_structure|
  data_structure_name = data_structure.split("/").last

  # Put the data in to the graph objects
  graph = Graph.new(REPEAT_SAME_TEST, data_structure_name)
  Dir[data_structure + "/*"].each do |file|
    Marshal.load(File.read(file)).each do |key, value|
      ds, nr_threads, nr_items = key
      data_points = value

      graph.add(nr_threads, nr_items, data_points)
    end
  end
  graphs[data_structure_name.to_sym] = graph
end

# Now all the data is in the graph, plot the graphs

# Overview
graphs[:cgl].plot(max_y: 25_000, file: "benchmark/report/figures/overview-cgl.pdf")
graphs[:fgl].plot(max_y: 25_000, file: "benchmark/report/figures/overview-fgl.pdf")
graphs[:cgt].plot(max_y: 25_000, file: "benchmark/report/figures/overview-cgt.pdf")
graphs[:fgt].plot(max_y: 25_000, file: "benchmark/report/figures/overview-fgt.pdf")

# Lists
graphs[:cgl].plot(max_y: 20_000, file: "benchmark/report/figures/list-cgl.pdf")
graphs[:fgl].plot(max_y: 20_000, file: "benchmark/report/figures/list-fgl.pdf")

# Trees
graphs[:cgt].plot(max_y:  25_000, file: "benchmark/report/figures/tree-cgt.pdf")
graphs[:fgt].plot(max_y:  25_000, file: "benchmark/report/figures/tree-fgt.pdf")

graphs[:cgt].plot(max_y:  200, file: "benchmark/report/figures/tree-cgt-8.pdf")

system("cd benchmark; pdflatex benchmark.tex")
