$LOAD_PATH << Dir.pwd
require "benchmark/config.rb"

module Runner
  extend self

  def calculate_jobs
    definitions = []

    job_count  = 0

    DATA_STRUCTURES.each do |data_structure|
      for nr_threads in THREADS
        for n in ITEMS
          job_count += 1
          job_work = [[data_structure, nr_threads, n]]

          job = {:data_file => "benchmark/data/#{data_structure}/#{nr_threads}-#{n}", :work => job_work}
          definition_file = "benchmark/jobs/#{job_count}.job"

          if File.exists?(job[:data_file])
            puts "Skipping #{job[:data_file]}"
          else
            puts "Defined #{job[:data_file]}"
            definitions.push definition_file
          end

          unless File.exists?(definition_file)
            File.open(definition_file, "w") do |job_def|
              job_def.puts Marshal.dump(job)
            end
          end
        end
      end
    end

    definitions
  end

  def run!
    definitions = calculate_jobs

    definitions.each_with_index do |definition, idx|
      puts "Running #{idx+1} of #{definitions.length}"
      if ENV["DAS"] == "false"
        puts "Running locally"
        system("ruby benchmark/benchmark.rb #{definition}")
      elsif ENV["DAS"] == "true"
        puts "Running on DAS"
        wait_for_empty_enough_queue
        system("bash -c 'prun -np 1 ruby benchmark/benchmark.rb #{definition}' &")
      else
        raise "You should pass DAS=true/false"
      end
    end
  end

  def wait_for_empty_enough_queue
    while (lines = `qstat`.split("\n").length) > 50
      $stderr.puts "Waiting for empty queue (#{lines})"
      sleep 10
    end
    $stderr.puts "Sufficient empty slots"
  end

  def benchmark! file
    STDOUT.sync = true
    puts "BENCHMARKING #{file}"
    job = Marshal.load(File.read(file))

    result = {}
    job[:work].each do |part|
      result[part] = []
      REPEAT_SAME_TEST.times do |i|
        print "  #{part.collect(&:inspect).join(", ")} [#{i}]: "
        log_file = "benchmark/log/#{file}-#{i}"

        system("mkdir -p #{File.dirname(log_file)}")
        system("bin/test_data_structures #{part.join(" ")} 1 > #{log_file}")
        time = `cat #{log_file} | tail -n2 | head -n1 | cut -c 7-`.to_i
        puts time
        result[part] << time
      end
    end

    dir = File.dirname(job[:data_file])
    `mkdir -p #{dir}`

    File.open(job[:data_file],"w") do |report|
      report.puts Marshal.dump(result)
    end
  end
end


if ARGV.length == 0
  Runner.run!
else
  Runner.benchmark!(ARGV.first)
end
