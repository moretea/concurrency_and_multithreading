\contentsline {section}{\numberline {1}Introduction}{3}
\contentsline {subsection}{\numberline {1.1}Approach to data structures}{3}
\contentsline {section}{\numberline {2}Linked Lists}{4}
\contentsline {subsection}{\numberline {2.1}CoarseGrainedList}{5}
\contentsline {subsubsection}{\numberline {2.1.1}The ``add'' method}{5}
\contentsline {subsubsection}{\numberline {2.1.2}The ``remove'' method}{6}
\contentsline {subsubsection}{\numberline {2.1.3}The ``toString'' method}{7}
\contentsline {subsection}{\numberline {2.2}FineGrainedList}{8}
\contentsline {subsubsection}{\numberline {2.2.1}The ``add'' method}{8}
\contentsline {subsubsection}{\numberline {2.2.2}The ``remove'' method}{10}
\contentsline {subsubsection}{\numberline {2.2.3}The ``toString'' method}{10}
\contentsline {section}{\numberline {3}Search trees}{11}
\contentsline {subsection}{\numberline {3.1}CoarseGrainedTree}{12}
\contentsline {subsubsection}{\numberline {3.1.1}Adding algorithm}{13}
\contentsline {subsubsection}{\numberline {3.1.2}Removing algorithm}{14}
\contentsline {subsubsection}{\numberline {3.1.3}The ``toString'' method}{16}
\contentsline {subsection}{\numberline {3.2}FineGrainedTree}{17}
\contentsline {subsubsection}{\numberline {3.2.1}Adding algorithm}{18}
\contentsline {subsubsection}{\numberline {3.2.2}Removing algorithm}{19}
\contentsline {section}{\numberline {4}Performance Hypotheses}{22}
