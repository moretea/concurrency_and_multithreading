\subsection{FineGrainedTree}
 As with the fine grained list, the tree has two types of locks: a lock on the root and locks for each node. At first, every thread acquires a lock on the root and the root node. These locks can be lifted while traversing deeper into the tree, in order to allow concurrent reading and writing in other parts of the tree.

 \begin{figure}[htb]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/fine_grained_tree}
    \caption{Class diagram of the FineGrainedTree}
    \label{fig:tree:fine:class}
  \end{figure}

\subsubsection{Adding algorithm}
  \begin{algorithm}[H]
    \caption{FineGrainedTree ``add'' algorithm}
    \begin{algorithmic}[1]
      \Function{add}{$tree$, $newValue$}
        \Lock{tree.rootLock}  \Comment{Lock the ``root'' of the tree.}
        \State $newNode \gets Node.new(newValue)$

        \If{$tree.root = null$}
          \State $tree.root \gets newNode$
          \Unlock{tree.rootLock}
        \Else
          \State $current \gets tree.root$
          \Lock{current.nodeLock}
          \Unlock{tree.rootLock}
          \Loop
            \If{$newValue \leq current.value$}  \Comment{Check the left child}
              \If{$current.left = nil$}         \Comment{We found a ready slot (LFT)}
                \State $current.left \gets newNode$ \Comment{Fill it (LFT)}
                \Unlock{current.nodeLock}
                \BreakLoop                      \Comment{And we're done (LFT)}
              \Else                             \Comment{Bummer; not found!}
                \State $newCurrent \gets current.left $
                \Lock{newCurrent.nodeLock}
                \Unlock{current.nodeLock}
                \State $current \gets newCurrent$ \Comment{Travel deeper in the tree}
                \ContinueLoop
              \EndIf
            \Else \Comment{Check the right child}
              \If{$current.right = nil$}         \Comment{We found a ready slot (RGT)}
                \State $current.right \gets newNode$ \Comment{Fill it (RGT)}
                \Unlock{current.nodeLock}
                \BreakLoop                      \Comment{And we're done (RGT)}
              \Else                             \Comment{Bummer; not found!}
                \State $newCurrent \gets current.right$
                \Lock{newCurrent.nodeLock}
                \Unlock{current.nodeLock}
                \State $current \gets newCurrent$ \Comment{Travel deeper in the tree}
                \ContinueLoop
              \EndIf
            \EndIf
          \EndLoop
        \EndIf
      \EndFunction
    \end{algorithmic}
  \end{algorithm}

\newpage

\subsubsection{Removing algorithm}
  We need to cover the same cases in the FineGrained Tree remove algorithm, as in 
  the removing algorithm for the CoarseGrainedTree algorithm.

  \begin{algorithm}[H]
    \caption{FineGrainedTree ``remove'' algorithm (Part I)}
    \begin{algorithmic}[1]
      \Function{remove}{$tree$, $removeValue$}
        \Lock{tree.rootLock}       \Comment{Lock the root of the tree}
        \Lock{tree.root.nodeLock}
        \If{$removeValue = tree.root.value$} \Comment{Remove the root}
          \State $tree.root.value \gets$ \Call{RmSmallestAndReturnValue}{$tree.root$}
          \Unlock{tree.root.nodeLock}
          \Unlock{tree.rootLock}
        \Else
          \State $current \gets tree.root$
          \State $parent  \gets null$
          \Loop
            \If{$removeValue \neq current.value$}
              \State $oldParent \gets parent$

              \If{$removeValue < current.value$}
                \State $parent \gets current$ \Comment{Digging deeper}
                \State $current \gets current.left$
              \ElsIf{$removeValue > current.value$}
                \State $parent \gets current$ \Comment{Digging deeper}
                \State $current \gets current.right$
              \EndIf

              \Lock{current.nodeLock}
              \If{$oldParent = null$}
                \Unlock{tree.rootLock}
              \Else
                \Unlock{oldParent.nodeLock}
              \EndIf
              \ContinueLoop
      \algstore{alg:trees:fine:remove}
    \end{algorithmic}
  \end{algorithm}

  \begin{algorithm}[H]
    \caption{FineGrainedTree ``remove'' algorithm (Part II)}
    \begin{algorithmic}[1]
      \algrestore{alg:trees:fine:remove}
            \ElsIf{$removeValue = current.value$} \Comment{Gotcha!}
              \If{$current.left = null$ \And $current.right = null$} \Comment{Leaf}
                \If{$parent.left = current$}      \Comment{Now, remove the}
                  \State $parent.left \gets null$ \Comment{child from }
                \ElsIf{$parent.right = current$} \Comment {the parent}
                  \State $parent.right \gets null$
                \EndIf
              \ElsIf{$current.left \neq null$ \And $current.right = null$}
                % Only left
                \If{$parent.left = current$}              \Comment{There is only}
                  \State $parent.left \gets current.left$ \Comment{a left child node}
                \ElsIf{$parent.right = current$}          
                  \State $parent.right \gets current.left$
                \EndIf
              \ElsIf{$current.left = null$ \And $current.right \neq null$}
                % Only right
                \If{$parent.left = current$}                \Comment{There is only}
                  \State $parent.left \gets current.right$  \Comment{a right child node}
                \ElsIf{$parent.right = current$}
                  \State $parent.right \gets current.right$
                \EndIf
              \ElsIf{$current.left \neq null$ \And $current.right \neq null$}
                % Inner node
                \State $current.value \gets$ \Comment{Inner node}
                \State $\Call{RmSmallestAndReturnValue}{current}$ 
              \EndIf

              \Unlock{current.nodeLock}
              \Unlock{parent.nodeLock}
            \EndIf % current.value == removeValue
          \EndLoop
        \EndIf

        \State \textbf{unlock}$(tree.globalLock)$  \Comment{Unlock tree}
      \EndFunction

      \algstore{alg:trees:fine:remove}
    \end{algorithmic}
  \end{algorithm}

  \begin{algorithm}[H]
    \caption{FineGrainedTree ``remove'' algorithm (Part III)}
    \begin{algorithmic}[1]
      \algrestore{alg:trees:fine:remove}

      % Finding the smallest is simply a matter of doing "left-recursion"
      % for as long as there is a next left child.
      \Function{RmSmallestAndReturnValue}{originalParent}
        \State $parent \gets originalParent$
        \State $result \gets parent.right$
        \Lock{result.nodeLock}
        \Loop
          \If{$result.left = null$} \Comment{Result is now the smallest value.}
             % Check if it has a right child node, if so we need to some more work.
            \If{$result.right \neq null$} \Comment{Result is an inner node.}
              % Note that since it's the smallest, it doesn't have a left node!
              \State $parent.left = result.right$ \Comment{Replace result by it's right}
            \ElsIf{$result.right = null$} \Comment{Result is leaf, easy!}
              \State $parent.left = null$ \Comment{Remove result and}
            \EndIf
            \Unlock{result.nodeLock}
            \If{$parent \neq originalParent$} \Comment{We can't unlock the original parent,}
              \Unlock{parent.nodeLock}        \Comment{since it is ``owned'' by the remove}
            \EndIf                            \Comment{algorithm.}
            \State \Return $result.value$ \Comment{return the value.}

          \Else                                 \Comment{There still is another left node,}
            \State $parent \gets result$        \Comment{which contains a smaller value}
            \State $result \gets parent.left $  \Comment{than the current result.}
            \Lock{result.nodeLock}
            \If{$parent \neq originalParent$}
              \Unlock{parent.nodeLock}
            \EndIf
          \EndIf % result.left = null
        \EndLoop
      \EndFunction
    \end{algorithmic}
  \end{algorithm}
