\subsection{CoarseGrainedTree}
  \begin{figure}[htb]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/coarse_grained_tree}
    \caption{Class diagram of the CoarseGrainedTree}
    \label{fig:tree:coarse:class}
  \end{figure}
  
  Like the coarse grained list, a thread needs to acquire a lock on the whole tree if it wants to write or remove a value.
  
  The following two cases should be considered at implementing an add algorithm:
  
  \begin{enumerate}
	\item The new node will be added to an empty tree
	\item The new node must be added to an existing leaf in the tree. The algorithm needs to traverse through the tree to find the leaf where a new node will be placed.
  \end{enumerate}
  
  The values in all nodes at the left sub tree of a node are smaller or equal than the value in the node itself. All values at the right sub tree of a node are larger than the value of the node.

\subsubsection{Adding algorithm}
  \begin{algorithm}[H]
    \caption{CoarseGrainedTree ``add'' algorithm}
    \begin{algorithmic}[1]
      \Function{add}{$tree$, $newValue$}
        \State \textbf{lock}$(tree.globalLock)$  \Comment{Lock the entire tree}
        \State $newNode \gets Node.new(newValue)$

        \If{$tree.root = null$}
          \State $tree.root \gets newNode$
        \Else
          \State $current \gets tree.root$
          \Loop
            \If{$newValue \leq current.value$}  \Comment{Check the left child}
              \If{$current.left = nil$}         \Comment{We found a ready slot (LFT)}
                \State $current.left \gets newNode$ \Comment{Fill it (LFT)}
                \BreakLoop                      \Comment{And we're done (LFT)}
              \Else                             \Comment{Bummer; not found!}
                \State $current \gets current.left$ \Comment{Travel deeper in the tree}
                \ContinueLoop
              \EndIf
            \Else \Comment{Check the right child}
              \If{$current.right = nil$}         \Comment{We found a ready slot (RGT)}
                \State $current.right \gets newNode$ \Comment{Fill it (RGT)}
                \BreakLoop                      \Comment{And we're done (RGT)}
              \Else                             \Comment{Bummer; not found!}
                \State $current \gets current.right$ \Comment{Travel deeper in the tree}
                \ContinueLoop
              \EndIf
            \EndIf
          \EndLoop
        \EndIf

        \State \textbf{unlock}$(tree.globalLock)$  \Comment{Unlock tree}
      \EndFunction
    \end{algorithmic}
  \end{algorithm}

\newpage

\subsubsection{Removing algorithm}
  When removing a node with a specified value from the tree, the node may have the following roles in the tree:
  \begin{enumerate}
    \item The node with the specified value is the root
    \item The node with the specified value is an ``full-inner'' node \\ (has both a left and a right child)
    \item The node with the specified value is an ``half-inner'' node \\ (has only a left or a right child)
    \item The node with the specified value is a leaf node
  \end{enumerate}

  \noindent We assume that the reader is familiar with the binary tree algorithms that are provided below.

  \begin{algorithm}[H]
    \caption{CoarseGrainedTree ``remove'' algorithm (Part I)}
    \begin{algorithmic}[1]
      \Function{remove}{$tree$, $removeValue$}
        \State \textbf{lock}$(tree.globalLock)$  \Comment{Lock the entire tree}

        \If{$removeValue = tree.root.value$} \Comment{Remove the root}
          \If{$tree.root.right \neq null$}
            \State $tree.root.value \gets$ \Call{RmSmallestAndReturnValue}{$tree.root$}
          \Else
            \State $tree.root = tree.left$
          \EndIf
        \Else
          \State $current \gets tree.root$
          \State $parent  \gets null$
          \Loop
            \If{$newValue < current.value$}
              \State $parent \gets current$
              \State $current \gets current.left$ \Comment{Digging deeper}
              \ContinueLoop
            \ElsIf{$newValue > current.value$}
              \State $parent \gets current$
              \State $current \gets current.right$ \Comment{Digging deeper}
              \ContinueLoop
            \ElsIf{$removeValue = current.value$} \Comment{Gotcha!}
              \If{$current.left = null$ \And $current.right = null$} \Comment{Leaf}
                \If{$parent.left = current$}   \Comment{Now, remove the}
                  \State $parent.left \gets null$ \Comment{child from }
                \ElsIf{$parent.right = current$} \Comment {the parent}
                  \State $parent.right \gets null$
                \EndIf
              \ElsIf{$current.left \neq null$ \And $current.right = null$}
                % Only left
                \If{$parent.left = current$}              \Comment{There is only}
                  \State $parent.left \gets current.left$ \Comment{a left child node}
                \ElsIf{$parent.right = current$}          
                  \State $parent.right \gets current.left$
                \EndIf
              \ElsIf{$current.left = null$ \And $current.right \neq null$}
                % Only right
                \If{$parent.left = current$}                \Comment{There is only}
                  \State $parent.left \gets current.right$  \Comment{a right child node}
                \ElsIf{$parent.right = current$}
                  \State $parent.right \gets current.right$
                \EndIf

              \ElsIf{$current.left \neq null$ \And $current.right \neq null$}
                % Inner node
                \State $current.value \gets$ \Comment{Inner node}
                \State $\Call{RmSmallestAndReturnValue}{current}$ 
              \EndIf
            \EndIf
          \EndLoop
        \EndIf

        \State \textbf{unlock}$(tree.globalLock)$  \Comment{Unlock tree}
      \EndFunction

      \algstore{alg:trees:coarse:remove}
    \end{algorithmic}
  \end{algorithm}

  \begin{algorithm}[H]
    \caption{CoarseGrainedTree ``remove'' algorithm (Part II)}
    \begin{algorithmic}[1]
      \algrestore{alg:trees:coarse:remove}

      % Finding the smallest is simply a matter of doing "left-recursion"
      % for as long as there is a next left child.
      \Function{RmSmallestAndReturnValue}{parent}

        \State $result \gets parent.right$
        \Loop
          \If{$result.left = null$} \Comment{Result is now the smallest value.}
             % Check if it has a right child node, if so we need to some more work.
            \If{$result.right \neq null$} \Comment{Result is an inner node.}
              % Note that since it's the smallest, it doesn't have a left node!
              \State $parent.left = result.right$ \Comment{Replace result by it's right}
              \State \Return $result.value$ \Comment{child and return the value.}
            \ElsIf{$result.right = null$} \Comment{Result is leaf, easy!}
              \State $parent.left = null$ \Comment{Remove result and}
              \State \Return $result.value$ \Comment{return the value.}
            \EndIf

          \Else                                 \Comment{There still is another left node,}
            \State $parent \gets result$        \Comment{which contains a smaller value}
            \State $result \gets parent.left $  \Comment{than the current result.}
          \EndIf % result.left = null
        \EndLoop
      \EndFunction
    \end{algorithmic}
  \end{algorithm}

\subsubsection{The ``toString'' method}
The toString method returns a textual representation of the tree that is used for debugging purposes.
