\subsection{CoarseGrainedList}
The CoarseGrainedList implements a ``single-core'' linked list algorithm, which is protected by a global lock. This means only one thread can read or write to the list at a time.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/coarse_grained_list}
  \caption{Class diagram of the CoarseGrainedList}
  \label{fig:list:coarse:class}
\end{figure}

The classes of the data structure are displayed in Figure \ref{fig:list:coarse:class}.
As can be seen, there are three methods that can be called on the CoarseGrainedList 
datastructure: the \textsl{add}, the \textsl{remove} and the \textsl{toString} methods.

The \textsl{toString} method is used for debugging purposes, and will only informally be described.

\subsubsection{The ``add'' method}
  There are four cases for the add method which will be considered for our implementation. Let $n$ be the amount of items in the list before adding the new value.

  \begin{enumerate}
    \item The new value will be added to an empty list.
    \item The new value must be added before the first item in the list.
    \item The new value must be added after the $1^{st}$, $2^{nd}$, ..., or $n-1^{th}$ value.
    \item The new value must be added at the end of the list.
  \end{enumerate}
  
The algorithm needed to add an item to the list can be described in pseudo code as in Algorithm \ref{alg:list:coarse:add}.

  \begin{algorithm}[H]
    \caption{CoarseGrainedList add algorithm}
    \begin{algorithmic}
      \Function{add}{$list$, $newValue$}
        \label{alg:list:coarse:add}
        \State $newLink \gets ListLink.new(newValue)$
        \State \textbf{lock}$(list.globalLock)$  \Comment{Coarse grained lock}
        \If{$list.head = null$}  \Comment{Case 1: the empty list}
          \State $list.head \gets newLink$
        \Else
          \If{$list.head.value \geq value$} \Comment{Case 2: new value before first link}
            \State $newLink.next \gets list.head$
            \State $list.head \gets newLink$
          \Else 
            \State $current \gets list.head$
            \State $next \gets list.head.next$

            \While{$next \not= null$}
              \If{$next.value \geq value$} \Comment{Case 3: middle of the list}
                \State $newLink.next \gets current.next$
                \State $current.next \gets newLink$
                \State \textbf{unlock}$(list.globalLock)$
                \State \Return
              \EndIf
              \State $current \gets next$
              \State $next \gets current.next$
            \EndWhile

            \State $current.next \gets newLink$
            \Comment{Case 4: at the end of the list}

          \EndIf
        \EndIf
        \State \textbf{unlock}$(list.globalLock)$
      \EndFunction
    \end{algorithmic}
  \end{algorithm}

\subsubsection{The ``remove'' method}
  There are two allowed cases for the remove method.
  
  \begin{enumerate}
    \item The value to remove is the first link.
    \item The value to remove is the value of the $1^{st}$ up to and including $n^{th}$ link.
  \end{enumerate}
  
  Because of the specifications states that no non-existing value will be removed, we do not have 
  to consider that case.
  
  If the value to remove occurs twice or more times in the list, the value will be 
  removed only once.

  \begin{algorithm}[H]
    \caption{CoarseGrainedList ``remove'' algorithm}
    \begin{algorithmic}
      \Function{remove}{$list$, $removeValue$}
        \State \textbf{lock}$(list.globalLock)$  \Comment{Coarse grained lock}
        \If{$list.head.value = removeValue$} \Comment{Case 1}
          \State $list.head \gets list.head.next$
          \State \textbf{unlock}$(list.globalLock)$
          \State \Return
        \Else
          \State $current \gets list.head$
          \State $next \gets current.next$
          \While{$next \neq null$} \Comment{ Case 2 }
            \If{$next.value = removeValue$}
              \State $current.next = next.next$
              \State \textbf{unlock}$(list.globalLock)$
              \State \Return
            \EndIf
            \State $current \gets next$
            \State $next \gets current.next$
          \EndWhile
        \EndIf
      \EndFunction
    \end{algorithmic}
  \end{algorithm}

\subsubsection{The ``toString'' method}
The toString method returns a textual representation of the list that is used for debugging purposes. The returned value is a line with the string representation of each value in the list, ordered from the first to the last node.
