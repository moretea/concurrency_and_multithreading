\subsection{FineGrainedList}
The FineGrainedList implements a linked list algorithm with two types of locks.

The first lock type is a single lock that locks the head of the list. This is the $headLock$ on the $LinkedList<T>$ class in 
Figure \ref{fig:list:fine:class}.
With this lock, we can safely start reading the list or add a new link at the beginning of the list.

The second type (Figure \ref{fig:list:fine:class}, $linkLock$ on the class $ListLink<T>$) is a lock on each link.  
If a link is locked by a thread, no other threads can read the value of the link, change the link to the next link or write a new value to the link.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/fine_grained_list}
  \caption{Class diagram of the FineGrainedList}
  \label{fig:list:fine:class}
\end{figure}

\subsubsection{The ``add'' method}
  The same four cases as discussed for the CoarseGrainedList apply to the FineGrainedList.
  We also want to emphasise that the algorithm takes the ``warnings'' into account that we posed in the introduction.

  Figure \ref{fig:list:fine:class} shows the classes needed for the algorithm., and the algorithm itself can be found in
  Algorithm \ref{alg:list:fine:add}.

  \begin{algorithm}[H]
    \caption{FineGrainedList add algorithm}
    \begin{algorithmic}
      \Function{add}{$list$, $newValue$}
        \label{alg:list:fine:add}
        \State $newLink \gets ListLink.new(newValue)$
        \State \textbf{lock}$(list.headLock)$  \Comment{Lock the "head"}
        \If{$list.head = null$}  \Comment{Case 1: the empty list}
          \State $list.head \gets newLink$
          \State \textbf{unlock}$(list.headLock)$
        \Else
          \State \textbf{lock}$(list.head.linkLock)$
          \If{$list.head.value \leq value$} \Comment{Case 2: new value before first link}
            \State $newLink.next \gets list.head$
            \State $list.head \gets newLink$
            \State \textbf{unlock}$(list.head.linkLock)$
            \State \textbf{unlock}$(list.headLock)$
          \Else 
            \State $current \gets list.head$
            \State $next \gets current.next$
            \State $unlocked\_head \gets false$

            \While{$next \not= null$}
              \State \textbf{lock}$(next.linkLock)$
              \If{\textbf{not} $unlocked\_head$}
                \State \textbf{unlock}$(list.headLock)$
                \State $unlocked\_head \gets true$
              \EndIf
              \If{$next.value \leq value$} \Comment{Case 3: middle of the list}
                \State $newLink.next \gets next$
                \State $current.next \gets newLink$
                \State \textbf{unlock}$(next.linkLock)$
                \State \textbf{unlock}$(current.linkLock)$
                \State \Return
              \Else
                \State \textbf{unlock}$(current.linkLock)$
                \State $current \gets next$
                \State $next \gets current.next$
              \EndIf
            \EndWhile

            \State $current.next \gets newLink$
            \Comment{Case 4: at the end of the list}
            \State \textbf{unlock}$(current.linkLock)$
            \If{\textbf{not} $unlocked\_head$} \Comment{Could have skipped while loop}
              \State \textbf{unlock}$(list.headLock)$ \Comment{Which happens when $n=1$}
              \State $unlocked\_head \gets true$
            \EndIf
          \EndIf
        \EndIf
      \EndFunction
    \end{algorithmic}
  \end{algorithm}

\subsubsection{The ``remove'' method}
  There are two cases for the remove method. Again, these are the same as for the coarse grained list.

  \begin{algorithm}[H]
    \caption{FineGrainedList ``remove'' algorithm}
    \begin{algorithmic}
      \Function{remove}{$list$, $removeValue$}
      \State \textbf{lock}$(list.headLock)$  \Comment{Head lock}
        \If{$list.head.value = removeValue$} \Comment{Case 1}
          \State $toRemove = list.head$
          \State \textbf{lock}$(toRemove.linkLock)$
          \State $list.head \gets list.head.next$
          \State \textbf{unlock}$(toRemove.linkLock)$
          \State \textbf{unlock}$(list.headLock)$
        \Else % HANDJES
          \State $current \gets list.head$
          \State \textbf{lock}$(current.linkLock)$
          \State $next \gets current.next$
          \State $unlocked\_head \gets false$

          \While{$next \neq null$} \Comment{ Case 2}
            \State \textbf{lock}$(next.linkLock)$
            \If{\textbf{not} $unlocked\_head$}
              \State \textbf{unlock}$(list.headLock)$
              \State $unlocked\_head \gets true$
            \EndIf
            \If{$next.value = removeValue$}
              \State $current.next = next.next$
              \State \textbf{unlock}$(next.linkLock)$
              \State \textbf{unlock}$(current.linkLock)$ and
              \Return
            \EndIf
            \State \textbf{unlock}$(current.linkLock)$
            \State $current \gets next$
            \State $next \gets current.next$
          \EndWhile \Comment {We should never exit this while ``naturally''}
        \EndIf \Comment{Therefore, we won't have to release any locks}
      \EndFunction
    \end{algorithmic}
  \end{algorithm}

\subsubsection{The ``toString'' method}
The toString method returns a textual representation that is used for debugging purposes. It should produce the same output as for an equivalent coarse grained list.
